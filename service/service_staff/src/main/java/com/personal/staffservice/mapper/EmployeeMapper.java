package com.personal.staffservice.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.personal.staffservice.entity.Employee;

import java.util.List;

public interface EmployeeMapper extends BaseMapper<Employee> {
    List<Employee> getEmployeeById(Integer id);
}
