package com.personal.staffservice.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.personal.staffservice.entity.Employee;
import com.personal.staffservice.service.EmployeeService;
import com.personal.utilcommon.R;
import io.swagger.annotations.Api;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Wrapper;
import java.util.List;

@RestController
@RequestMapping("/first")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public R getEmployeeList(){
        List<Employee> list = employeeService.list();
        return R.ok().data("list",list);
    }

    //测试自己的sql
    @PostMapping("/getEmployeeById/{id}")
    public R getEmployeeById(@PathVariable Integer id){
        return R.ok().data("list",employeeService.getEmployeeById(id));
    }

    @PostMapping("/saveEmployee")
    public R saveEmployee(Employee employee){
        boolean save = employeeService.save(employee);
        return R.ok().data("save",save);
    }

    @DeleteMapping("/removeEmployee/{id}")
    public R removeEmployee(@PathVariable Integer id){
        boolean remove = employeeService.removeById(id);
        return R.ok().data("remove",remove);
    }
}
