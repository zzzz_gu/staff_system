package com.personal.staffservice.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.personal.staffservice.entity.Employee;
import org.springframework.stereotype.Service;

import java.util.List;


public interface EmployeeService extends IService<Employee> {
    List<Employee> getEmployeeById(Integer id);
}
