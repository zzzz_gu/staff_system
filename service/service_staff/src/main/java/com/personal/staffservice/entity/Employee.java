package com.personal.staffservice.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.ibatis.annotations.Delete;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Data
//set的时候可以返回对象本身，用于链式编程
//fluent，生成的get和set不带get,set
//prefix 可以将符合驼峰命名的属性的 前部分去除
@Accessors(chain = true)
//callSuper 表示重写equals和hashcode方法
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "员工信息表")
public class Employee implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "主键")
    @TableId(type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "部门id")
    private Integer depId;

    @ApiModelProperty(value = "职位id,默认为0")
    private Integer roleId;

    @ApiModelProperty(value = "姓名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "手机号")
    private String telephone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "头像,默认为空")
    private String avatar;

    @ApiModelProperty(value = "年龄，默认为0")
    private Integer age;

    @ApiModelProperty(value = "性别，0代表女，1代表男，默认为0")
    private Integer sex;

    @ApiModelProperty(value = "学历,默认为空")
    private String eduBg;

    @ApiModelProperty(value = "底薪，默认为0")
    private Integer salary;

    @ApiModelProperty(value = "出生日期,默认为0")
    private LocalDateTime birthday;

    @ApiModelProperty(value = "状态,0代表正常，1代表解雇，默认为0")
    @TableLogic
    private Integer status;

    @ApiModelProperty(value = "本，默认为0")
    @Version
    private Integer version;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
}
