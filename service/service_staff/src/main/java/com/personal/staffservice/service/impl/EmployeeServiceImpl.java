package com.personal.staffservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.personal.staffservice.entity.Employee;
import com.personal.staffservice.mapper.EmployeeMapper;
import com.personal.staffservice.service.EmployeeService;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.List;

@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {
    //覆盖方法，一次最多访问100条数据
    public List<Employee> list() {
        QueryWrapper<Employee> queryWrapper=new QueryWrapper<>();
        queryWrapper.last("limit 0,100");
        return super.list(queryWrapper);
    }

    @Override
    public List<Employee> getEmployeeById(Integer id) {
        return baseMapper.getEmployeeById(id);
    }
}
