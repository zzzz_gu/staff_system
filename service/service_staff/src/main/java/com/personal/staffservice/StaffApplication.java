package com.personal.staffservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.personal"})
public class StaffApplication {
    public static void main(String[] args) {
        try{
            SpringApplication.run(StaffApplication.class,args);
        }catch (Exception e){
            System.out.println("nihao");
        }

    }
}
