package com.personal.staffservice;

import com.personal.staffservice.entity.Employee;
import com.personal.staffservice.service.EmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.Date;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MyTest {

    @Autowired
    EmployeeService employeeService;

    @Test
    public void setlectEmployeeList(){
        for (Employee employee : employeeService.list()) {
            System.out.println(employee);
        }
    }

    @Test
    public void deleteEmployee(){
        boolean remove = employeeService.removeById(2);
    }

    @Test
    public void addEmployee(){
        Employee employee=new Employee();
        employee.setAge( 11).setAvatar("2").setDepId( 1).setEduBg("1").setEmail("18@33com").setPassword("123")
                .setTelephone("123733991").setUsername("123");
        employeeService.save(employee);
    }

}
