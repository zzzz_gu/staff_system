package com.personal.utilcommon;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

//一定要有get,set
@Data
@Accessors(chain = true)
public class R {

    @ApiModelProperty(value = "是否成功")
    private boolean success;

    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String message;

    @ApiModelProperty(value = "返回数据")
    private Map<String,Object> data=new HashMap<>();

    private R(){}

    public R data(String key,Object value){
        data.put(key,value);
        return this;
    }

    public R success(boolean success){
        return setSuccess(success);
    }

    public R code(Integer code){
        return setCode(code);
    }

    public R message(String message){
       return setMessage(message);
    }




    public static R ok(){
        return new R().setSuccess(true).setCode(ResultCode.SUCCESS).setMessage("成功");
    }

    public static R error(){
        return new R().setSuccess(false).setCode(ResultCode.ERROR).setMessage("失败");
    }
}
