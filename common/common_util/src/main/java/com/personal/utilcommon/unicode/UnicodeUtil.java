package com.personal.utilcommon.unicode;

import com.personal.utilcommon.unicode.FileUtil;

import java.io.*;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//将文件里的Unicode编码更改为对应的字符(中文)
public class UnicodeUtil {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入要修改的文件绝对路径：");
        String path=sc.nextLine();
        fileCodeReplace(path);
    }

    //将文件里的Unicode编码转换为对应字符
    public static void fileCodeReplace(String path){
        //获取文件内容
        System.out.println("开始获取文件内容...");
        String content = FileUtil.readFileContent(path);
        if(content!=null && !content.trim().equals(""))
            System.out.println("获取文件内容成功！");
        else
            System.out.println("获取文件内容失败！");

        //将Unicode编码转换成对应字符
        String newContent = codeReplace(content);

        //将新内容写入path对应文件的副本
        System.out.println("文件生成中...");
        File file=FileUtil.createNewFile(path);
        if(file!=null){
            FileUtil.writeFileContent(file,newContent);
            System.out.println("目标文件根目录下,新文件生成完毕");
            System.out.println("文件名为:");
            System.out.println(file.getName());
            System.out.println("路径为:");
            System.out.println(file.getAbsolutePath());
        }
        else
            System.out.println("文件生成失败!");

    }

    //将里面的Unicode编码字符转换为正常字符(中文字符)
    public static String codeReplace(String str){
        String regex="\\\\u(\\w{4})";
        Pattern pattern=Pattern.compile(regex);
        Matcher matcher = pattern.matcher(str);
        //如 group: \u7AEF ,group2: 7AEF ,ch 为group按16进制转换的字符
        while (matcher.find()){
            String group = matcher.group(0);
            String group2 = matcher.group(1);
            char ch= (char) Integer.parseInt(group2,16);
            str=str.replace(group,ch+"");
        }
        return str;
    }


}
