package com.personal.utilcommon.unicode;

import java.io.*;

public class FileUtil {
    //创建文件的副本
    public static File createNewFile(String path){
        File file=new File(path);
        //判断是否为文件
        if(file.isFile()){
            StringBuilder builder=new StringBuilder(path);
            //保证新路径不存在
            do{
                int i = builder.lastIndexOf(".");
                builder.insert(i,"-copy");
                file=new File(builder.toString());
            }while (file.exists());
            //进行创建
            try {
                file.createNewFile();
            } catch (IOException ioException) {
                //创建失败，将file更改为Null
                file=null;
                ioException.printStackTrace();
            }
            return file;
        }
        //返回空值，防止使用将原文件覆盖
        return null;
    }

    //将内容写入文件中
    public static void writeFileContent(File file,String content){
        BufferedWriter writer=null;
        try{
            writer=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
            writer.write(content);
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                writer.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    //根据路径获取文件内容
    public static String readFileContent(String path) {
        BufferedReader reader=null;
        //储存文件内容
        StringBuilder builder=new StringBuilder();
        try {
            reader=new BufferedReader(new InputStreamReader(new FileInputStream(path)));
            String line="";
            while ((line=reader.readLine())!=null){
                builder.append(line+"\n");
            }
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            try {
                reader.close();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
        //返回文件内容
        return builder.toString();
    }
}
